from math import pi, sin, cos

from direct.showbase.ShowBase import ShowBase
from direct.task import Task
from direct.actor.Actor import Actor
from direct.interval.IntervalGlobal import Sequence
from panda3d.core import Point3
from panda3d.core import Shader
from panda3d.core import loadPrcFileData 

from random import randint

from blocks6 import print_plan

class MyApp(ShowBase):
    
    def __init__(self):
        ShowBase.__init__(self)

        # Disable the camera trackball controls.
        self.disableMouse()

        # Load the environment model.
        self.scene = self.loader.loadModel("/c/Users/grigo/3D Objects/room.gltf")
        # Reparent the model to render.
        self.scene.reparentTo(self.render)
        # Apply scale and position transforms on the model.
        self.scene.setScale(0.5, 0.5, 0.5)
        self.scene.setPos(0, 0, -3.0)

        self.container = self.loader.loadModel("/c/Users/grigo/3D Objects/container.gltf")
        # Reparent the model to render.
        self.container.reparentTo(self.render)
        # Apply scale and position transforms on the model.
        self.get_plan()
#        self.plan_generate()
        (rx, ry, rz) = self.plan[-1]
        self.plan.remove(self.plan[-1])
        
        self.container.setScale(rx/2, ry/2, rz/2)
        self.container.setPos(rx/2, ry/2, rz/2)
        self.container.setTransparency(True)
        self.container.setColorScale(0.0, 0.0, 0.0, 0.3)
        
        # Add the spinCameraTask procedure to the task manager.
        self.taskMgr.add(self.spinCameraTask, "SpinCameraTask")
##        
##        self.vase = self.loader.load_model("/c/Users/grigo/3D Objects/vase.gltf")
##        self.vase.reparent_to(self.render)
##
##        self.tree = self.loader.load_model("/c/Users/grigo/3D Objects/tree.gltf")
##        self.tree.reparent_to(self.render)
##
##        self.closet = self.loader.load_model("/c/Users/grigo/3D Objects/closet.gltf")
##        self.closet.reparent_to(self.render)
##
##        self.crate = self.loader.load_model("/c/Users/grigo/3D Objects/crate.gltf")
##        self.crate.reparent_to(self.render)

##        self.plan_generate()
        
        self.object_create()
        self.j = []
        for t in self.objects:
            scale, place_0, place, model = t
            file, _ = self.types[model]
            x = self.loader.load_model(file)
            x.reparentTo(self.render)
            x.setScale(scale)
            x.setPos(place_0)
            x.setColor(1,1,1,1)
            self.j.append((x,place))
            print("Placed:")
            print(x.getPos())
            print(place, model, scale)
            
        self.taskMgr.add(self.build, "BuildTask")
    # Define a procedure to move the camera.
    def spinCameraTask(self, task):
        angleDegrees = task.time * 6.0
        angleRadians = angleDegrees * (pi / 180.0)
        self.camera.setPos(20 * sin(angleRadians), -20 * cos(angleRadians), 3)
        self.camera.setHpr(angleDegrees, 0, 0)
        return Task.cont

    def build(self, task):
        k = task.time/1.5
        if(k >= len(self.j)):
            k = len(self.j) - 1
            
        x, place = self.j[int(k)]
        x.setPos(place)
        return Task.cont    
    
    def plan_generate(self):
        self.plan = []
##        for i in range(3):
##            self.plan.append(((randint(5,40), randint(5,40), randint(5,40)), (randint(5,40), randint(5,40), randint(5,40))))
        self.plan = [((0.0, 0.0, 0.0), (1.0, 1.0, 1.0)), ((1.0, 1.0, 0.0), (1.0, 1.0, 1.0)), ((0.0, 1.0, 0.0), (1.0, 1.0, 1.0)), ((2.0, 2.0, 0.0), (1.0, 1.0, 1.0)) , (4.0, 4.0, 2.0) ]    
    def get_plan(self):
        self.plan = print_plan()
        print("this is plan")
        print(self.plan)
        
    def object_create(self):
        self.types = {}
        self.objects = []
        self.types["vase"] = ("/c/Users/grigo/3D Objects/vase.gltf" , (1.0, 1.0, 1.0))
##        self.types["tree"] = ("/c/Users/grigo/3D Objects/tree.gltf" , (1.0, 1.0, 3.5))
        self.types["closet"] = ("/c/Users/grigo/3D Objects/closet.gltf" , (3.5, 2.0, 1.3))
        self.types["crate"] = ("/c/Users/grigo/3D Objects/crate3.gltf" , (1.0, 1.0, 1.0))
        d = self.types.keys()
        d = list(d)
        coord_x = -5.0
        coord_y = 0.0
        for t in self.plan:
            (place_x, place_y, place_z), (size_x, size_y, size_z) = t
##            (place_x, place_y, place_z), (size_x, size_y, size_z) = (place_x/5, place_y/5, place_z/5), (size_x/5, size_y/5, size_z/5)
            
            place = (place_x + size_x*0.5, place_y + size_y*0.5 , place_z + size_z*0.5)
            m = randint(0, len(d) - 1)
            model = d[m]
            _, (x0, y0, z0) = self.types[model]
            scale = (size_x/x0, size_y/y0, size_z/z0)
            if ((coord_x + size_x) > 5.0):
                coord_x = -5.0
                coord_y += 3.0
            else:
                coord_x += size_x + 0.5
            place_0 = (coord_x + size_x*0.5, coord_y + size_y*0.5, 0.0 + size_z*0.5)
            self.objects.append((scale, place_0, place, model))

loadPrcFileData('', 'win-size 1024 768') 
app = MyApp()
app.run()
