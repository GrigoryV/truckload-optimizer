from math import pi, sin, cos

from direct.showbase.ShowBase import ShowBase
from direct.task import Task
from direct.actor.Actor import Actor
from direct.interval.IntervalGlobal import Sequence
from panda3d.core import Point3
from panda3d.core import Shader
from panda3d.core import loadPrcFileData 


class MyApp(ShowBase):
    def __init__(self):
        ShowBase.__init__(self)

        # Disable the camera trackball controls.
        self.disableMouse()

        # Load the environment model.
        self.scene = self.loader.loadModel("models/environment")
        # Reparent the model to render.
        self.scene.reparentTo(self.render)
        # Apply scale and position transforms on the model.
        self.scene.setScale(0.25, 0.25, 0.25)
        self.scene.setPos(-8, 42, 0)

        # Add the spinCameraTask procedure to the task manager.
        self.taskMgr.add(self.spinCameraTask, "SpinCameraTask")
        
        self.vase = self.loader.load_model("/c/Users/grigo/3D Objects/vase.gltf")
        self.vase.reparent_to(self.render)

        self.tree = self.loader.load_model("/c/Users/grigo/3D Objects/tree.gltf")
        self.tree.reparent_to(self.render)

        self.closet = self.loader.load_model("/c/Users/grigo/3D Objects/closet.gltf")
        self.closet.reparent_to(self.render)

        self.crate = self.loader.load_model("/c/Users/grigo/3D Objects/crate.gltf")
        self.crate.reparent_to(self.render)
        
    # Define a procedure to move the camera.
    def spinCameraTask(self, task):
        angleDegrees = task.time * 6.0
        angleRadians = angleDegrees * (pi / 180.0)
        self.camera.setPos(20 * sin(angleRadians), -20 * cos(angleRadians), 3)
        self.camera.setHpr(angleDegrees, 0, 0)
        return Task.cont
    def object_create(self, plan):
        for t in plan

loadPrcFileData('', 'win-size 1024 768') 
app = MyApp()
app.run()
