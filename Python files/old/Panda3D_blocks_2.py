from math import pi, sin, cos

from direct.showbase.ShowBase import ShowBase
from direct.task import Task
from direct.actor.Actor import Actor
from direct.interval.IntervalGlobal import Sequence
from panda3d.core import Point3
from panda3d.core import Shader
from panda3d.core import loadPrcFileData 

from random import randint

class MyApp(ShowBase):
    
    def __init__(self):
        ShowBase.__init__(self)

        # Disable the camera trackball controls.
        self.disableMouse()

        # Load the environment model.
        self.scene = self.loader.loadModel("models/environment")
        # Reparent the model to render.
        self.scene.reparentTo(self.render)
        # Apply scale and position transforms on the model.
        self.scene.setScale(0.25, 0.25, 0.25)
        self.scene.setPos(-8, 42, 0)
        
        # Add the spinCameraTask procedure to the task manager.
        self.taskMgr.add(self.spinCameraTask, "SpinCameraTask")
##        
##        self.vase = self.loader.load_model("/c/Users/grigo/3D Objects/vase.gltf")
##        self.vase.reparent_to(self.render)
##
##        self.tree = self.loader.load_model("/c/Users/grigo/3D Objects/tree.gltf")
##        self.tree.reparent_to(self.render)
##
##        self.closet = self.loader.load_model("/c/Users/grigo/3D Objects/closet.gltf")
##        self.closet.reparent_to(self.render)
##
##        self.crate = self.loader.load_model("/c/Users/grigo/3D Objects/crate.gltf")
##        self.crate.reparent_to(self.render)
        self.plan_generate()
        self.object_create()
        for t in self.objects:
            scale, place, model = t
            file, _ = self.types[model]
            x = self.loader.load_model(file)
            x.reparentTo(self.render)
            x.setScale(scale)
            x.setPos(place)
            
        
    # Define a procedure to move the camera.
    def spinCameraTask(self, task):
        angleDegrees = task.time * 6.0
        angleRadians = angleDegrees * (pi / 180.0)
        self.camera.setPos(20 * sin(angleRadians), -20 * cos(angleRadians), 3)
        self.camera.setHpr(angleDegrees, 0, 0)
        return Task.cont
    
    def plan_generate(self):
        self.plan = []
        for i in range(3):
            self.plan.append(((randint(1,4), randint(1,4), randint(1,4)), (randint(1,4), randint(1,4), randint(1,4))))
            
    
    def object_create(self):
        self.types = {}
        self.objects = []
        self.types["vase"] = ("/c/Users/grigo/3D Objects/vase.gltf" , (1.0, 1.0, 1.0))
        self.types["tree"] = ("/c/Users/grigo/3D Objects/tree.gltf" , (1.0, 1.0, 3.5))
        self.types["closet"] = ("/c/Users/grigo/3D Objects/closet.gltf" , (3.5, 2.0, 1.3))
        self.types["crate"] = ("/c/Users/grigo/3D Objects/crate.gltf" , (1.0, 1.0, 1.0))
        d = self.types.keys()
        d = list(d)
        for t in self.plan:
            (size_x, size_y, size_z), (place_x, place_y, place_z) = t
            (place_x, place_y, place_z) = (place_x + size_x/2, place_y + size_y/2, place_z + size_z/2)
            place = (place_x, place_y, place_z)
            m = randint(0, len(d) - 1)
            model = d[m]
            _, (x0, y0, z0) = self.types[model]
            scale = (size_x/x0, size_y/y0, size_z/z0)
            self.objects.append((scale, place, model))

loadPrcFileData('', 'win-size 1024 768') 
app = MyApp()
app.run()
