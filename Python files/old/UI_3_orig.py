import tkinter as tk
import time


k1 = ('Little', 'Medium', 'Big', 'Large' )
a1s = []
t1s = []
lists = []
a2s = []
t2s = []
spin1s = []
opts = []
data = []
sizes = {}
sizes["Little"] = (5, 5, 5)
sizes['Medium'] = (10, 10, 5)
sizes['Big'] = (20, 10 , 10)
sizes['Large'] = (20, 30, 40)

def return_data():
    return data

def more(event):
    a1s.append(tk.Frame(frame_box))
    a1 = a1s[-1]
    a1.pack(side = tk.TOP)
    t1s.append(tk.Label(a1, text = 'Choose size:'))
    t1 = t1s[-1]
    t1.pack(side = tk.LEFT)

    opts.append(tk.StringVar())
    opt1 = opts[-1]
    opt1.set(k1[0])
    lists.append(tk.OptionMenu(a1,opt1,*k1 ))
    lists[-1].pack(side = tk.LEFT)
    
    a2s.append(tk.Frame(frame_box)) 
    a2s[-1].pack(side = tk.TOP)
    a2 = a2s[-1]
    t2s.append(tk.Label(a2, text = 'How many?'))
    t2s[-1].pack(side = tk.LEFT)
    spin1s.append(tk.Spinbox(a2, from_= 1, to_= 40))
    spin1s[-1].pack(side = tk.LEFT)
def pack(event):
    data[:] = []
    for x in range(len(a1s)):
        name = opts[x].get()        
        k = int(spin1s[x].get())
        for y in range(k):
            data.append(sizes[name])
    print(data)
    return_data()
root = tk.Tk()
root.title('Pack up')
frame_box = tk.LabelFrame(root,text = 'Box selection', labelanchor = 'nw')
frame_box.pack(side = tk.LEFT)

button_add = tk.Button(frame_box, text = 'Add')
button_add.pack(side = tk.TOP)

a1s.append(tk.Frame(frame_box))
a1 = a1s[-1]
a1.pack(side = tk.TOP)
t1s.append(tk.Label(a1, text = 'Choose size:'))
t1 = t1s[-1]
t1.pack(side = tk.LEFT)

opts.append(tk.StringVar())
opt1 = opts[-1]
opt1.set(k1[0])
lists.append(tk.OptionMenu(a1,opt1,*k1 ))
lists[-1].pack(side = tk.LEFT)
    
a2s.append(tk.Frame(frame_box)) 
a2s[-1].pack(side = tk.TOP)
a2 = a2s[-1]
t2s.append(tk.Label(a2, text = 'How many?'))
t2s[-1].pack(side = tk.LEFT)
spin1s.append(tk.Spinbox(a2, from_= 1, to_= 40))
spin1s[-1].pack(side = tk.LEFT)


frame_car = tk.LabelFrame(root,text = 'Delivery selection', labelanchor = 'n')
frame_car.pack(side = tk.LEFT)
b1 = tk.Frame(frame_car)
b1.pack(side = tk.TOP)
t3 = tk.Label(b1, text = 'Choose options:')
t3.pack(side = tk.LEFT)
check5 = tk.Checkbutton(b1, text = 'Fast')
check5.pack(side = tk.LEFT)
check6 = tk.Checkbutton(b1, text = 'Cheap')
check6.pack(side = tk.LEFT)
b2 = tk.Frame(frame_car)
b2.pack(side = tk.TOP)
t3 = tk.Label(b2, text = 'Choose delivery:')
t3.pack(side = tk.LEFT)
k2 = ('to home', 'to store', 'to work')
opt2 = tk.StringVar()
opt2.set(k2[0])
list2 = tk.OptionMenu(b2,opt2,*k2 )
list2.pack(side = tk.LEFT)
b3 = tk.Frame(frame_car)
b3.pack(side = tk.TOP)
t4 = tk.Label(b3, text = 'What would be the perfect date?')
t4.pack(side = tk.TOP)

b4 = tk.Frame(frame_car)
b4.pack(side = tk.TOP)
spin2 = tk.Spinbox(b4, from_= 1, to_= 31)
spin2.pack(side = tk.LEFT)
k3 = ('January', 'February', 'March', 'April', 'May', 'June', 'July', 'August', 'September', 'October', 'November', 'December')
opt3 = tk.StringVar()
opt3.set(k3[0])
list3 = tk.OptionMenu(b4,opt3,*k3 )
list3.pack(side = tk.LEFT)


frame_ready = tk.Frame(root)
frame_ready.pack(side = tk.LEFT)
d3 = tk.Frame(frame_ready)
d3.pack(side = tk.TOP)
button3 = tk.Button(d3, text = '?')
button3.pack(side = tk.RIGHT)
t5 = tk.Label(d3, text = 'Ready')
t5.pack(side = tk.RIGHT)
d1 = tk.Frame(frame_ready)
d1.pack(side = tk.TOP)

c1 = tk.Frame(frame_ready)
c1.pack(side = tk.TOP)
k3 = ('Car','Truck', 'Van', 'Cargo Plane','Ship')
opt3 = tk.StringVar()
opt3.set(k3[0])
list3 = tk.OptionMenu(c1,opt3,*k3 )
list3.pack(side = tk.LEFT)


d2 = tk.Frame(frame_ready)
d2.pack(side = tk.TOP)
button1 = tk.Button(d2, text = 'Save')
button1.pack(side = tk.LEFT)
button2 = tk.Button(d2, text = 'Print')
button2.pack(side = tk.LEFT)

button_add.bind('<Button-1>',more)
button2.bind('<Button-1>',pack)

root.mainloop()
