import direct.directbase.DirectStart
from direct.showbase import DirectObject
from panda3d.core import *

class Test(DirectObject.DirectObject):
    def __init__(self):
        self.accept("FireZeMissiles", self._fireMissiles)

    def _fireMissiles(self):
        print("Missiles fired! Oh noes!")

    # function to get rid of me
    def destroy(self):
        self.ignoreAll()

foo = Test()  # create our test object
foo.destroy() # get rid of our test object

del foo

messenger.send("FireZeMissiles") # No missiles fire
base.run()
