import tkinter as tk
import time
import webbrowser


k1 = ('Little', 'Medium', 'Big', 'Large' )
k2 = ('January', 'February', 'March', 'April', 'May', 'June', 'July', 'August', 'September', 'October', 'November', 'December')
k3 = ('Car','Truck', 'Van', 'Cargo Plane','Ship')
a1s = []
t1s = []
lists = []
a2s = []
t2s = []
spin1s = []
opts = []
data = []
spin_range = []
sizes = {}
sizes['Little'] = (0.5, 0.5, 0.5)
sizes['Medium'] = (1.0, 1.0, 1.0)
sizes['Big'] = (2.0, 1.0 , 1.0)
sizes['Large'] = (2.0, 3.0, 4.0)

cargo = {'Car': (2.0, 1.5, 1.0), 'Truck': (5.0, 10.0, 3.0), 'Van':(4.0, 4.0, 2.0), 'Cargo Plane':(30.0, 60.0, 10.0), 'Ship':(40.0, 60.0, 30.0) }

flag = True

def help(event):
    webbrowser.open("https://grigoryvydrevich.wixsite.com/truckload-optimizer")

def return_data():
    return data

def more():
    for lab in k1:
        
        a2s.append(tk.Frame(frame_box)) 
        a2s[-1].pack(side = tk.TOP)
        a2 = a2s[-1]
        
        t2s.append(tk.Label(a2, text = lab))
        t2s[-1].pack(side = tk.LEFT)
        spin1s.append(tk.Spinbox(a2, from_= 0, to_= 40))
        spin1s[-1].pack(side = tk.LEFT)
        opts.append(lab)
        
def pack(event):
    data[:] = []
    data.append((length.get(),width.get(), height.get()))
    for x in range(len(spin1s)):
        name = opts[x]
        k = int(spin1s[x].get())
        for y in range(k):
            data.append(sizes[name])
    print(data)
    return_data()

def default(event):
    (x,y,z) = cargo[opt3.get()]
    width.set(x)
    length.set(y)
    height.set(z)

def stop(event):
    flag = False
    root.destroy()
root = tk.Tk()
root.title('Pack up')
frame_box = tk.LabelFrame(root,text = 'Box selection', labelanchor = 'nw')
frame_box.pack(side = tk.TOP)

width = tk.DoubleVar()
height = tk.DoubleVar()
length = tk.DoubleVar()

more()

frame_car = tk.LabelFrame(root,text = 'Delivery selection', labelanchor = 'n')
frame_car.pack(side = tk.TOP)
b1 = tk.Frame(frame_car)
b1.pack(side = tk.TOP)
t3 = tk.Label(b1, text = 'Choose options:')
t3.pack(side = tk.LEFT)
check5 = tk.Checkbutton(b1, text = 'Fast')
check5.pack(side = tk.LEFT)
check6 = tk.Checkbutton(b1, text = 'Cheap')
check6.pack(side = tk.LEFT)
b2 = tk.Frame(frame_car)
b2.pack(side = tk.TOP)
t3 = tk.Label(b2, text = 'Choose delivery:')
t3.pack(side = tk.LEFT)
k4 = ('to home', 'to store', 'to work')
opt4 = tk.StringVar()
opt4.set(k2[0])
list4 = tk.OptionMenu(b2,opt4,*k4 )
list4.pack(side = tk.LEFT)
b3 = tk.Frame(frame_car)
b3.pack(side = tk.TOP)
t4 = tk.Label(b3, text = 'What would be the perfect date?')
t4.pack(side = tk.TOP)

b4 = tk.Frame(frame_car)
b4.pack(side = tk.TOP)
spin2 = tk.Spinbox(b4, from_= 1, to_= 31)
spin2.pack(side = tk.LEFT)

opt2 = tk.StringVar()
opt2.set(k2[0])
list2 = tk.OptionMenu(b4,opt2,*k2 )
list2.pack(side = tk.LEFT)

c1 = tk.Frame(frame_car)
c1.pack(side = tk.TOP)

opt3 = tk.StringVar()
opt3.set(k3[0])
list3 = tk.OptionMenu(c1,opt3,*k3 )
list3.pack(side = tk.LEFT)

c2 = tk.Frame(frame_car)
c2.pack(side = tk.TOP)

x = tk.Frame(c2)
x.pack(side = tk.TOP)
tx = tk.Label(x, text = 'length:')
tx.pack(side = tk.LEFT)
spin_range.append(tk.Spinbox(x, from_= 1.0, to_= 70.0, format = '%.1f', increment = 0.1, textvariable = length))
spin_range[-1].pack(side = tk.LEFT)

y = tk.Frame(c2)
y.pack(side = tk.TOP)
ty = tk.Label(y, text = 'width:')
ty.pack(side = tk.LEFT)
spin_range.append(tk.Spinbox(y, from_= 1.0, to_= 70.0, format = '%.1f', increment = 0.1, textvariable = width))
spin_range[-1].pack(side = tk.LEFT)

z = tk.Frame(c2)
z.pack(side = tk.TOP)
tz = tk.Label(z, text = 'heigth')
tz.pack(side = tk.LEFT)
spin_range.append(tk.Spinbox(z, from_= 1.0, to_= 70.0, format = '%.1f', increment = 0.1, textvariable = height))
spin_range[-1].pack(side = tk.LEFT)

frame_ready = tk.Frame(root)
frame_ready.pack(side = tk.TOP)
d3 = tk.Frame(frame_ready)
d3.pack(side = tk.TOP)
button3 = tk.Button(d3, text = '?')
button3.pack(side = tk.RIGHT)
t5 = tk.Label(d3, text = 'Ready')
t5.pack(side = tk.RIGHT)
d1 = tk.Frame(frame_ready)
d1.pack(side = tk.TOP)



d2 = tk.Frame(frame_ready)
d2.pack(side = tk.TOP)
button1 = tk.Button(d2, text = 'Save')
button1.pack(side = tk.LEFT)
button2 = tk.Button(d2, text = 'Print')
button2.pack(side = tk.LEFT)

list3.bind('<Button-1>',default)
button2.bind('<Button-1>', pack)
button1.bind('<Button-1>', stop)
button3.bind('<Button-1>', help)


root.mainloop()
